﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Runtime.InteropServices;

public class CalculatorScript : MonoBehaviour 
{
    [DllImport("DTest")]
    private static extern float FooPluginFunction(float x, float y);

    public Text x, y, r;

    public void DoAddition()
    {
        float _x = float.Parse(x.text);
        float _y = float.Parse(y.text);

        float _r = FooPluginFunction(_x, _y);

        r.text = _r.ToString();
    }
}
